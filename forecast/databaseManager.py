from abc import abstractmethod


class DatabaseManager():
    """
    Abstract class used as an Interface for managing database.
    """

    @abstractmethod
    def getAllMeasurements(self):
        """
        :return: All Measurements from database.
        """
        pass

    @abstractmethod
    def createMeasurement(self, temp, ozone, wind, pressure, measurementTime=None):
        """
        Saves new measurement to database.
        :param temperature: Floating number for saving temperature.
        :param ozone: Floating number for saving ozone.
        :param windSpeed: Floating number for saving speed of wind.
        :param pressure:Floating number for saving pressure.
        :param time: Datetime parameter determines date and time of measurement, default is now minus one day.
        """
        pass

    @abstractmethod
    def getAllForecasts(self):
        """
        :return: All Forecasts from database.
        """
        pass

    @abstractmethod
    def createForecast(self, temp, ozone, wind, pressure, measurementTime=None):
        """
        Saves new forecast to database.
        :param temperature: Floating number for saving temperature.
        :param ozone: Floating number for saving ozone.
        :param windSpeed: Floating number for saving speed of wind.
        :param pressure:Floating number for saving pressure.
        :param time: Datetime parameter determines date and time of forecast,
                default is now plus defaultForecastDistance from config.
        """
        pass

    @abstractmethod
    def clearForecasts(self):
        """
        Removes all Forecast objects from database.
        """
        pass

    @abstractmethod
    def clearMeasurements(self):
        """
        Removes all Measurement objects from database.
        """
        pass
