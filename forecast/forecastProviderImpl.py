import urllib
import json
from configurationImpl import ConfigurationImpl
from constants import DEFAULT_API_KEY, DEFAULT_LATITUDE, DEFAULT_LONGITUDE, DEFAULT_FORECAST_DISTANCE, PROPERTIES, \
    UNIX_DAY, SERVICE_URL
from forecastProvider import ForecastProvider


class ForecastProviderImpl(ForecastProvider):
    """
    Class for providing forecast precision.
    """

    @classmethod
    def getForecast(cls, actuallTime):
        """
        Retrieve forecast data from dark-sky server.
        :param actuallTime:  Actual time in unix epoch.
        :return: tuple of real yesterday's data and prediction data
        """

        yesterdayTime = int(actuallTime) - UNIX_DAY
        yesterday = cls.__getData(str(yesterdayTime))

        daysPrediction = ConfigurationImpl.getValueMaybe('data', 'forecastdistance', DEFAULT_FORECAST_DISTANCE)
        predictionTime = int(actuallTime) + int(daysPrediction) * UNIX_DAY
        prediction = cls.__getData(str(predictionTime))

        return (yesterday, prediction)

    @classmethod
    def __getData(cls, time):
        """
        Gets data from json of dark.sky api.
        Needs service url, localization and apikey, works with json.
        :param time:  time in unix epoche
        :return: tuple of properties' data
        """

        lat = ConfigurationImpl.getValueMaybe('location', 'latitude', DEFAULT_LATITUDE)
        long = ConfigurationImpl.getValueMaybe('location', 'longitude', DEFAULT_LONGITUDE)
        key = ConfigurationImpl.getValueMaybe('data', 'apikey', DEFAULT_API_KEY)

        url = SERVICE_URL + key + '/' + lat + ',' + long + ',' + time
        uh = urllib.urlopen(url)
        data = uh.read()
        try:
            js = json.loads(str(data))
        except:
            js = None

        tup = ()
        for item in PROPERTIES:
            data = js["currently"][item]
            tup = tup + (data,)
        return tup
