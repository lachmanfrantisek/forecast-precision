import logging

from forecastiopy import FIODaily, ForecastIO
from daemonRun import DaemonRun
from dataGenerator import DataGenerator
from logBuilder import LogBuilder
import time
import forecastProviderImpl

apikey = '07a431f99de6abd892507ee90622659e'

place = [50.0755, 14.4378]

wanted = ['time', 'temperatureMax', 'pressure', 'ozone', 'humidity']

fio = ForecastIO.ForecastIO(apikey,
                            units=ForecastIO.ForecastIO.UNITS_SI,
                            lang=ForecastIO.ForecastIO.LANG_ENGLISH,
                            latitude=place[0], longitude=place[1],
                            extend='daily')


def get_info(day):
    daily = FIODaily.FIODaily(fio)
    info = daily.get_day(day)
    tup = ()
    for item in wanted:
        data = info[item]
        tuple_data = (data,)
        tup = tup + tuple_data
    return tup



###############################################

from configurationImpl import ConfigurationImpl

print(ConfigurationImpl.isValid())
print(ConfigurationImpl.getConfig()['data']['apikey'])
l = LogBuilder.getLogger("tst")

l.debug('dbg')
l.warn('wrn')

#####################################################

DataGenerator.clearDatabase()
#DataGenerator.makeTestingData(14)


from forecastProviderImpl import getForecast

print getForecast(time.time())


dR = DaemonRun()
dR.measurmentToDatabase()