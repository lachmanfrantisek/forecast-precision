import logging

CONFIG_FILE = 'forecast.conf'

LOG_FILE = 'forecast.log'
DEFAULT_LOG_LEVEL = logging.DEBUG

# apikey for dark-sky
DEFAULT_API_KEY = '07a431f99de6abd892507ee90622659e'
DEFAULT_FORECAST_DISTANCE = 2
SERVICE_URL = 'https://api.forecast.io/forecast/'

# in hours
DEFAULT_INTERVAL = 120

# day in seconds
UNIX_DAY = 86400

# location
DEFAULT_LATITUDE = 49.195060
DEFAULT_LONGITUDE = 16.606837

DEFAULT_PORT = 8000

PROPERTIES = ['time', 'temperature', 'windSpeed', 'pressure', 'ozone']
