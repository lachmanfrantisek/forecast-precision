import os
import django
from datetime import datetime, timedelta
from databaseManagerImpl import DatabaseManagerImpl
from unittest import TestCase

# Nutne pro pristup k django projektu
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "forecast.settings")
django.setup()
from forecast_web.models import Measurement, Forecast


class TestDatabaseManagerImpl(TestCase):
    dbMan = DatabaseManagerImpl()

    def setUp(self):
        Measurement.objects.all().delete()
        Forecast.objects.all().delete()
        super(TestDatabaseManagerImpl, self).setUp()

    def doCleanups(self):
        Measurement.objects.all().delete()
        Forecast.objects.all().delete()
        return super(TestDatabaseManagerImpl, self).doCleanups()

    def test_getAllMeasurements(self):
        for i in range(5):
            fromDb = self.dbMan.getAllMeasurements()
            directly = list(Measurement.objects.all().iterator())
            Measurement.objects.create(temperature=1,
                                       ozone=2,
                                       windSpeed=3,
                                       pressure=4)
            assert fromDb == directly

    def test_createMeasurement(self):
        for i in range(5):
            countBefore = Measurement.objects.all().count()
            self.dbMan.createMeasurement(temperature=1,
                                         ozone=2,
                                         windSpeed=3,
                                         pressure=4)
            self.dbMan.createMeasurement(time=datetime.now(),
                                         temperature=1,
                                         ozone=2,
                                         windSpeed=3,
                                         pressure=4)
            countAfter = Measurement.objects.all().count()
            assert countBefore + 2 == countAfter

    def test_getAllForecast(self):
        for i in range(5):
            fromDb = self.dbMan.getAllForecasts()
            directly = list(Forecast.objects.all().iterator())
            Forecast.objects.create(temperature=1,
                                    ozone=2,
                                    windSpeed=3,
                                    pressure=4)
            assert fromDb == directly

    def test_createForecast(self):
        for i in range(5):
            countBefore = Forecast.objects.all().count()
            self.dbMan.createForecast(time=datetime.now(),
                                      temperature=1,
                                      ozone=2,
                                      windSpeed=3,
                                      pressure=4)
            self.dbMan.createForecast(temperature=2,
                                      ozone=2,
                                      windSpeed=3,
                                      pressure=4)
            countAfter = Forecast.objects.all().count()
            assert countBefore + 2 == countAfter
