from abc import abstractmethod


class Configuration():
    """
    Abstract class used as a Interface for configuration.
    """

    @abstractmethod
    def isValid(cls):
        """
        :return: True, if the configuration is valid.
        """
        pass

    @abstractmethod
    def createNew(cls):
        """
        Creates new configuration with default values.
        """
        pass

    @abstractmethod
    def getConfig(cls):
        """
        Returns Config object - can be use as a dictionary.
        """
        pass

    @abstractmethod
    def loadFile(cls):
        """
        Loads configuration file.
        """
        pass

    @classmethod
    def getValueMaybe(cls, section, variable, defaultValue):
        """
        Tries to get value from configuration, if it is not successful, then the default is used.
        :param section: Name of section in config file.
        :param variable: Name of variable in section.
        :param defaultValue: Default value, which is used when retrieving value from config was not successful.
        :return: value from config, if it is not successful, then the default is used.
        """
        pass
