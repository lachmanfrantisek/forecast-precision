import os
import django
from datetime import timedelta, datetime
from configurationImpl import ConfigurationImpl
from constants import DEFAULT_FORECAST_DISTANCE
from databaseManager import DatabaseManager
from logBuilder import LogBuilder

# Necessary for connecting to django project.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "forecast.settings")
django.setup()

from forecast_web.models import Measurement, Forecast


class DatabaseManagerImpl(DatabaseManager):
    """
    Class for managing database.
    """

    logger = LogBuilder.getLogger('databasemanager')

    def getAllMeasurements(self):
        """
        :return: All Measurements from database.
        """

        self.logger.debug('Returning all measurements.')
        return list(Measurement.objects.all().iterator())

    def createMeasurement(self, temperature, ozone, windSpeed, pressure, time=None):
        """
        Saves new measurement to database.
        :param temperature: Floating number for saving temperature.
        :param ozone: Floating number for saving ozone.
        :param windSpeed: Floating number for saving speed of wind.
        :param pressure:Floating number for saving pressure.
        :param time: Datetime parameter determines date and time of measurement, default is now minus one day.
        """
        self.logger.debug('Creating new measurement.')
        if time is None:
            time = datetime.now() + timedelta(days=-1)
            self.logger.debug('No time was set on creating measurement, using default.')
        Measurement.objects.create(time=time,
                                   temperature=temperature,
                                   ozone=ozone,
                                   windSpeed=windSpeed,
                                   pressure=pressure)

    def getAllForecasts(self):
        """
        :return: All Forecasts from database.
        """
        self.logger.debug('Returning all forecasts.')
        return list(Forecast.objects.all().iterator())

    def createForecast(self, temperature, ozone, windSpeed, pressure, time=None):
        """
        Saves new forecast to database.
        :param temperature: Floating number for saving temperature.
        :param ozone: Floating number for saving ozone.
        :param windSpeed: Floating number for saving speed of wind.
        :param pressure:Floating number for saving pressure.
        :param time: Datetime parameter determines date and time of forecast,
                default is now plus defaultForecastDistance from config.
        """

        self.logger.debug('Creating new forecast.')
        if time is None:
            defaultForecastInstance = ConfigurationImpl.getValueMaybe('data', 'forecastdistance',
                                                                      DEFAULT_FORECAST_DISTANCE)
            time = datetime.now() + timedelta(days=int(defaultForecastInstance))
            self.logger.debug('No time was set on creating forecast, using default.')
        Forecast.objects.create(time=time,
                                temperature=temperature,
                                ozone=ozone,
                                windSpeed=windSpeed,
                                pressure=pressure)

    def clearForecasts(self):
        """
        Removes all Forecast objects from database.
        """
        Forecast.objects.all().delete()

    def clearMeasurements(self):
        """
        Removes all Measurement objects from database.
        """
        Measurement.objects.all().delete()
