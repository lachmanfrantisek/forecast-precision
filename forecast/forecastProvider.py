from abc import abstractmethod


class ForecastProvider:
    """
    Class for providing forecast precision.
    """

    @abstractmethod
    def getForecast(cls, actuallTime):
        """
        Retrieve forecast data from dark-sky server.
        :param actuallTime:  Actual time in unix epoch.
        :return: tuple of real yesterday's data and prediction data
        """
        pass

    @abstractmethod
    def getData(cls, time):
        """
        Gets data from json of dark.sky api.
        Needs service url, localization and apikey, works with json.
        :param time:  time in unix epoche
        :return: tuple of properties' data
        """
        pass
