from configurationImpl import ConfigurationImpl
from databaseManagerImpl import DatabaseManagerImpl
from datetime import datetime, timedelta
from logBuilder import LogBuilder


class DataGenerator():
    """
    Class for generating testing measurements and forecast.
    """

    d = DatabaseManagerImpl()
    logger = LogBuilder.getLogger('data-generator')

    @classmethod
    def makeTestingData(cls, daysNumber):
        """
        Generetes forecast and measurement for given days and interval from configuration.
        :param daysNumber: Number of days to simulate with generating.
        """
        cls.logger.debug('Genereating measurements for {} days.'.format(daysNumber))
        dateInterval = int(ConfigurationImpl.getValueMaybe('data', 'forecastdistance', 2))
        minuteInterval = int(ConfigurationImpl.getValueMaybe('data', 'interval', 2))
        tm = datetime.now()
        for i in range(daysNumber):
            for j in range(24 / minuteInterval):
                hour = - j * minuteInterval
                cls.d.createForecast(2.0 / (i + j + 1.1), 3.3 * (i * j), j * i, i,
                                     tm + timedelta(days=-i + dateInterval, hours=hour))
                cls.d.createMeasurement(3.5 / (i + 1.1) % 5, 3.0 / (1.1 + i + j), 7.0 / (j * i % 7 + 1.3), i % 3.1,
                                        tm + timedelta(days=-i - 1, hours=hour))

    @classmethod
    def clearDatabase(cls):
        """
        Clears all measurements and forecast from database.
        :return:
        """
        cls.d.clearForecasts()
        cls.d.clearMeasurements()
