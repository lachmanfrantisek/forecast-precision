from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^forecast/$', views.measurement_precision, name='measurement_precision'),
    url(r'^$', views.measurement_list, name='measurement_list'),
]
