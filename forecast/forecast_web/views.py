from datetime import timedelta, datetime
from django.db.models import Avg
from django.shortcuts import render

from configurationImpl import ConfigurationImpl
from constants import DEFAULT_FORECAST_DISTANCE, DEFAULT_INTERVAL, PROPERTIES
from logBuilder import LogBuilder
from .models import Measurement, Forecast

__logger = LogBuilder.getLogger('web.views')
__dateInterval = timedelta(
    days=int(ConfigurationImpl.getValueMaybe('data', 'forecastdistance', DEFAULT_FORECAST_DISTANCE)))
__minuteInterval = timedelta(minutes=int(ConfigurationImpl.getValueMaybe('data', 'interval', DEFAULT_INTERVAL)))


# Create your views here.

def measurement_list(request):
    """
    Generates html file with table of measurements and forecasts in given period.
    :param request: HTTP request with from and to parameters in querystring.
        If not set, last 365 days are used. And week in future.
    :return: html file with all measurements and forecasts.
    """
    __logger.debug('Request for all measurements.')

    fromTime = request.GET.get('from', datetime.now() - timedelta(days=365))
    toTime = request.GET.get('to', datetime.now() + timedelta(days=7))

    measurements = list(Measurement.objects \
                        .filter(time__lte=toTime) \
                        .filter(time__gte=fromTime).iterator())

    forecasts = list(Forecast.objects \
                     .filter(time__lte=toTime) \
                     .filter(time__gte=fromTime).iterator())

    return render(request, 'forecast_web/measurement_list.html', {'measurements': measurements,
                                                                  'forecast': forecasts,
                                                                  'from': fromTime,
                                                                  'to': toTime})


def measurement_precision(request):
    """
    Calculates precision of forecast in given interval.
    :param request: HTTP request with from and to parameters in querystring.
        If not set, last 365 days are used.
    :return: html file with precision info.
    """

    fromTime = request.GET.get('from', datetime.now() - timedelta(days=365))
    toTime = request.GET.get('to', datetime.now())

    __logger.debug('Getting request for precision from {} to {}.'.format(fromTime, toTime))

    measurements = list(Measurement.objects \
                        .filter(time__lte=toTime) \
                        .filter(time__gte=fromTime).iterator())

    __logger.debug('Found {} measurements.'.format(len(measurements)))

    forecasts = []
    for m in measurements:
        f = __getForecastForMeasurement(m)
        if f is not None:
            forecasts.append(f)

    __logger.debug('Found {} forecasts.'.format(len(forecasts)))

    avgDict = {}
    message = ''
    if len(forecasts) == 0:
        __logger.debug('No data for forecast precision.')
        message = 'No data'
    else:
        avgDict = {k: __roundAndAddPercentage(sum(d[k] for d in forecasts) / len(forecasts)) for k in forecasts[0]}
        __logger.debug('Average precision: ' + str(avgDict))

    return render(request, 'forecast_web/measurement_precision.html', {'from': fromTime,
                                                                       'to': toTime,
                                                                       'message': message,
                                                                       'precision': avgDict})


def __roundAndAddPercentage(x):
    """
    Rounds a number and concat it with percentage character.
    :param x: number to format
    :return: Formated number to print it on site.
    """
    return str(round(x, 2)) + '%'


def __getForecastForMeasurement(measurement):
    """
    Returns dictionary with forecast values for given measurement.
    :param measurement: Measurements to find forecast for.
    :return: dictionary with average forecast values
    """
    mTime = measurement.time
    fDict = Forecast.objects.filter(time__lte=mTime + __minuteInterval) \
        .filter(time__gte=mTime - __minuteInterval) \
        .aggregate(Avg(PROPERTIES[1]), Avg(PROPERTIES[2]), Avg(PROPERTIES[3]), Avg(PROPERTIES[4]))

    for k, v in fDict.iteritems():
        if v is None:
            __logger.debug('No forecast found for {}.'.format(measurement))
            return None

    avgDict = {}
    for type in PROPERTIES[1:]:
        avgDict[type] = __getPrecisionOfTwoValues(getattr(measurement, type), fDict[type + '__avg'])

    return avgDict


def __getPrecisionOfTwoValues(a, b):
    """
    Calculates precision of given two values.
    :param a: Value from Measurement.
    :param b: Value from Forecast.
    :return: Number from 0 to 1 showing precision of forecast.
    """
    if a > 0 and b < 0:
        a -= b
        b = 0
    elif a < 0 and b > 0:
        b -= a
        a = 0
    elif a == b and a == 0:
        return 100.0

    return 100.0 * (1.0 - abs(a - b) / abs(a + b))
