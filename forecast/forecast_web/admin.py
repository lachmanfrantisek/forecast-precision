from django.contrib import admin

# Register your models here.
from .models import Measurement, Forecast

admin.site.register(Measurement)
admin.site.register(Forecast)