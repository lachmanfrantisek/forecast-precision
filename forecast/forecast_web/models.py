from __future__ import unicode_literals

from datetime import datetime
from django.db import models


# Create your models here.

class Measurement(models.Model):
    """ Object for representing values of real weather state in database.
    """
    time = models.DateTimeField(default=datetime.now())
    temperature = models.FloatField()
    ozone = models.FloatField()
    windSpeed = models.FloatField()
    pressure = models.FloatField()

    def __str__(self):
        return "Mereni : <" + str(self.time) + ">"


class Forecast(models.Model):
    """ Object for representing prediction of values in database.
    """
    time = models.DateTimeField(default=datetime.now())
    temperature = models.FloatField()
    ozone = models.FloatField()
    windSpeed = models.FloatField()
    pressure = models.FloatField()

    def __str__(self):
        return "Predpoved: <" + str(self.time) + ">"
