# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-11 19:35
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forecast_web', '0002_auto_20160510_2340'),
    ]

    operations = [
        migrations.RenameField(
            model_name='forecast',
            old_name='temp',
            new_name='temperature',
        ),
        migrations.RenameField(
            model_name='forecast',
            old_name='wind',
            new_name='windSpeed',
        ),
        migrations.RenameField(
            model_name='measurement',
            old_name='temp',
            new_name='temperature',
        ),
        migrations.RenameField(
            model_name='measurement',
            old_name='wind',
            new_name='windSpeed',
        ),
        migrations.RemoveField(
            model_name='forecast',
            name='measurementTime',
        ),
        migrations.RemoveField(
            model_name='measurement',
            name='measurementTime',
        ),
        migrations.AddField(
            model_name='forecast',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 11, 19, 35, 36, 919778)),
        ),
        migrations.AddField(
            model_name='measurement',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 11, 19, 35, 36, 919065)),
        ),
    ]
