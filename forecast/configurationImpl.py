import configparser
import os

from constants import CONFIG_FILE, DEFAULT_LOG_LEVEL, DEFAULT_API_KEY, DEFAULT_INTERVAL, \
    DEFAULT_FORECAST_DISTANCE, DEFAULT_LONGITUDE, DEFAULT_LATITUDE, DEFAULT_PORT
from configuration import Configuration
from logBuilder import LogBuilder


class ConfigurationImpl(Configuration):
    """
    Static class for representing configuration.
    """

    config = configparser.ConfigParser()
    isLoaded = False
    logger = LogBuilder.getBasicLogger("configuration")

    @classmethod
    def isValid(cls):
        """
        :return: True, if the configuration is valid.
        """

        if not cls.isLoaded:
            cls.logger.debug("Need to load config file.")
            cls.loadFile()

        validity = True
        sections = cls.config.sections()
        cls.logger.debug("Testing config validity.")

        if "data" not in sections:
            cls.logger.warning("Wrong settings in config file - missing data section.")
            validity = False

        if "location" not in sections:
            cls.logger.warning("Wrong settings in config file - missing location section.")
            validity = False

        if "webserver" not in sections:
            cls.logger.warning("Wrong settings in config file - missing webserver section.")
            validity = False

        if "logging" not in sections:
            cls.logger.warning("Wrong settings in config file - missing logging section.")
            validity = False

        if validity:
            cls.logger.debug("Config is valid.")

        return validity

    @classmethod
    def createNew(cls):
        """
        Creates new configuration with default values.
        """
        cls.config['data'] = {'apikey': DEFAULT_API_KEY,
                              'interval': DEFAULT_INTERVAL,
                              'forecastdistance': DEFAULT_FORECAST_DISTANCE}

        cls.config['location'] = {'longitude': DEFAULT_LONGITUDE,
                                  'latitude': DEFAULT_LATITUDE}

        cls.config['webserver'] = {'port': DEFAULT_PORT}

        cls.config['logging'] = {'level': DEFAULT_LOG_LEVEL}

        with open(CONFIG_FILE, 'w') as configfile:
            cls.config.write(configfile)

        cls.logger.info("New config file created.")

    @classmethod
    def getConfig(cls):
        """
        Returns Config object - can be use as a dictionary.
        """

        if not cls.isLoaded:
            cls.logger.debug("Need to load config file.")
            cls.loadFile()

        if cls.isValid():
            cls.logger.debug("Returning valid config.")
            return cls.config
        else:
            cls.logger.warning("Config not valid.")
            return None

    @classmethod
    def loadFile(cls):
        """
        Loads configuration file.
        """

        if not os.path.exists(CONFIG_FILE):
            cls.createNew()

        cls.config.read(CONFIG_FILE)
        cls.logger.debug('Config loaded.')
        cls.isLoaded = True

    @classmethod
    def getValueMaybe(cls, section, variable, defaultValue):
        """
        Tries to get value from configuration, if it is not successful, then the default is used.
        :param section: Name of section in config file.
        :param variable: Name of variable in section.
        :param defaultValue: Default value, which is used when retrieving value from config was not successful.
        :return: value from config, if it is not successful, then the default is used.
        """
        if cls.isValid() and (section in cls.config):
            return cls.config[section].get(variable, defaultValue)
        return defaultValue
