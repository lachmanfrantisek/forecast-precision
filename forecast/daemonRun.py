import time

from forecastProviderImpl import ForecastProviderImpl
from databaseManagerImpl import DatabaseManagerImpl
from logBuilder import LogBuilder


class DaemonRun:
    """
    Class of daemon which collects the data.
    """
    logger = LogBuilder.getLogger('daemonRun')

    def fahrToCelsius(self, fahr):
        """
        Parse Fahrenheit degrees to Celsius.
        :param fahr: Value in degrees of Fahrenheit.
        :return: Value in degrees of Celsius.
        """
        return (fahr - 32) * (float(5) / 9)

    def measurmentToDatabase(self):
        """
        Saves the measurements and forecasts to database.
        """

        self.logger.debug('getting weather messurment from api')
        tup = ForecastProviderImpl.getForecast(time.time())

        dbMan = DatabaseManagerImpl()

        self.logger.debug('putting messurment to database')
        dbMan.createMeasurement(temperature=self.fahrToCelsius(tup[0][1]),
                                windSpeed=tup[0][2],
                                pressure=tup[0][3],
                                ozone=tup[0][4])
        self.logger.debug('putting forecast to database')
        dbMan.createForecast(temperature=self.fahrToCelsius(tup[1][1]),
                             windSpeed=tup[1][2],
                             pressure=tup[1][3],
                             ozone=tup[1][4])
