import logging

from constants import LOG_FILE, DEFAULT_LOG_LEVEL


class LogBuilder():
    @staticmethod
    def getLogger(name):
        from configurationImpl import ConfigurationImpl
        level = ConfigurationImpl.getValueMaybe('logging', 'level', DEFAULT_LOG_LEVEL)

        logger = logging.getLogger('forecast.' + name)
        logger.setLevel(level)

        logHandler = logging.FileHandler(LOG_FILE)
        logHandler.setLevel(level)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logHandler.setFormatter(formatter)

        logger.addHandler(logHandler)

        return logger

    @staticmethod
    def getBasicLogger(name):
        level = DEFAULT_LOG_LEVEL

        logger = logging.getLogger('forecast.' + name)
        logger.setLevel(level)

        logHandler = logging.FileHandler(LOG_FILE)
        logHandler.setLevel(level)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logHandler.setFormatter(formatter)

        logger.addHandler(logHandler)

        return logger
